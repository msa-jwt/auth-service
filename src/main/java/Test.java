
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;

import egovframework.com.cmm.LoginVO;
import egovframework.com.utl.sim.service.EgovFileScrty;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

public class Test {
	static String secretKey = "93w6IFwaMf3lG3671RWzvvmkdQjJgEwqlPIz4NTib20=";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		createKey();
//		createToken();
		
		testEgovPw();

	}
	
	private static void testEgovPw() {
		LoginVO vo = new LoginVO();
		vo.setId("lee");
		vo.setPassword("1111");
		try {
			String enpassword = EgovFileScrty.encryptPassword(vo.getPassword(), vo.getId());
			System.out.println(enpassword);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createKey() {
		// TODO Auto-generated method stub
		SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		String secretString = Encoders.BASE64.encode(key.getEncoded());
		System.out.println(secretString);
		
	}

	// https://github.com/jwtk/jjwt
	private static void createToken() {
		// TODO Auto-generated method stub
		Map<String, Object> header = new HashMap<>();
		header.put("typ", "JWT");
		header.put("alg", "HS256");
		
		
		Map<String, Object> payloads = new HashMap<>();
		long expireTime = 60 * 1000; // 1분
		Date now = new Date(System.currentTimeMillis ( ));
		now.setTime(now.getTime() + expireTime);
		payloads.put("exp", now);
		payloads.put("data", "test");
		
		
		
		String jwt = Jwts.builder()
				.setHeader(header)
				.setClaims(payloads)
				.signWith(SignatureAlgorithm.HS256, secretKey.getBytes(StandardCharsets.UTF_8))
				.compact();
				
		System.out.println(jwt);
		
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmb28iLCJpYXQiOjE1OTM1Njg0NDksImV4cCI6MTU5MzU3MjA0OX0.pj4zMMj_JCyBSpx18NbKlTqDeTo75y_G7ZB3AwOyovk";
		validToken(jwt);
	}

	private static void validToken(String jwt) {
		
		Claims claims = Jwts
				.parser()
				.setSigningKey(secretKey.getBytes(StandardCharsets.UTF_8))
				.parseClaimsJws(jwt)
				.getBody();
		
		try {
			Date expiration = claims.get("exp",Date.class);
			System.out.println(expiration);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String data = claims.get("data", String.class);
			System.out.println(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	

}
