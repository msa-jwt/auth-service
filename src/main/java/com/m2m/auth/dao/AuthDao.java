package com.m2m.auth.dao;

import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.m2m.auth.model.AuthUser;
import com.m2m.auth.model.AuthUserDetails;


@Mapper
public interface AuthDao {
	
	/*
		select emplyr_id as username, password, email_adres as email from comtnemplyrinfo where emplyr_id = 'admin'
		;
	 */
	Optional<AuthUser> selectUserOneById(String userId);

}
