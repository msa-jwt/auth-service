package com.m2m.auth.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.m2m.auth.dao.AuthDao;
import com.m2m.auth.model.AuthUser;
import com.m2m.auth.model.AuthUserDetails;

@Service
public class UserService {
	
	@Autowired
	private AuthDao authDao;
	
	public Optional<AuthUser>  getUserById(String userId) {
		return authDao.selectUserOneById(userId);
		

	}


}
