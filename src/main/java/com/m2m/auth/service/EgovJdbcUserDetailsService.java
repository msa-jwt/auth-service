package com.m2m.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.m2m.auth.dao.AuthDao;
import com.m2m.auth.model.AuthUser;
import com.m2m.auth.model.AuthUserDetails;


import java.util.ArrayList;
import java.util.Optional;


/**
 * 전자 정부 연동 모듈 
 * 전자정부 테이블에서 사용자 정보를 읽어서 사용자 인증을 처리함 
 */
@Service
public class EgovJdbcUserDetailsService implements UserDetailsService {
	
	
    @Autowired
    AuthDao authDao;

    @Override
    public AuthUserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        Optional<AuthUser> authUser = authDao.selectUserOneById(userId);

        authUser.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userId));

        return authUser.map(AuthUserDetails::new).get();
    }
    
}