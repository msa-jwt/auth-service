package com.m2m.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.m2m.auth.filter.JwtRequestFilter;
import com.m2m.auth.service.EgovJdbcUserDetailsService;



@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private EgovJdbcUserDetailsService jdbcUserDetailsService;
	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jdbcUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity
//        .csrf()
//            .disable()
//        .headers()
//            .disable()
//        .exceptionHandling()
////            .accessDeniedHandler(accessDeniedHandler)
////            .authenticationEntryPoint(unauthorizedHandler)
//            .and()
//        .sessionManagement()
//            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//            .and()
//        .authorizeRequests()
//            .antMatchers("/api/auth/**").permitAll()
//            .antMatchers("/api/**").hasRole(role)
//            .anyRequest().permitAll()
//            .and()
//        .formLogin()
//            .disable();
        
		httpSecurity
				.csrf()
					.disable()
				.authorizeRequests()
					.antMatchers(
							"/v2/api-docs",
			                "/configuration/ui",
			                "/swagger-resources/**",
			                "/configuration/security",
			                "/swagger-ui.html",
			                "/webjars/**"
							).permitAll()
					.antMatchers("/api/v1/auth/token/create").permitAll()
				.anyRequest()
					.authenticated().and()
				.formLogin()
					.disable()
				.exceptionHandling().and()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

	}

}