package com.m2m.auth.rest;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.m2m.auth.model.AuthUser;
import com.m2m.auth.model.AuthUserDetails;
import com.m2m.auth.service.EgovJdbcUserDetailsService;
import com.m2m.auth.service.UserService;
import com.m2m.auth.utils.JwtUtil;

import egovframework.com.utl.sim.service.EgovFileScrty;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
	
	@Value("${prop.auth.oauth.url}")
    private String OAUTH_URL ;
	
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;
	
	
	@Autowired
	private UserService userService;

	@Autowired
	private EgovJdbcUserDetailsService egovJdbcUserDetailsService;

	@RequestMapping(value= "/hello", method = RequestMethod.GET )
	public String firstPage() {
		return "Hello World";
	}

	/**
	 * 토큰생성 메소드
	 * 사용자로 부터 아이디와 패스워드를 입력 받아 인가된 사용자의 경우 토큰을 생성하여 반환함 
	 * @param authUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/token/create", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthUser authUser) throws Exception {

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authUser.getUsername(), authUser.getEncPw())
			);
		}
		catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}


		final AuthUserDetails authUserDetails = egovJdbcUserDetailsService
				.loadUserByUsername(authUser.getUsername());

		final String jwt = jwtTokenUtil.generateTokenWithClaim(authUserDetails);
		
		Optional<AuthUser> userVo = userService.getUserById(authUser.getUsername());
		String name = userVo.map(AuthUser::getName).orElse("");
		String userId = userVo.map(AuthUser::getUsername).orElse("");
		
		/*
		 * aouth call
		 */
		
		if("oauth".equals(authUser.getAuthType().toLowerCase())) {
			final String uri = "http://"+OAUTH_URL+"/api/v1/external/auth/"+userId;
			RestTemplate restTemplate = new RestTemplate();
			
			Map result = restTemplate.getForObject(uri, Map.class);
			log.debug("RE : "+result.toString());
		}

		
		log.debug("Auth_Type:"+authUser.getAuthType());

		return ResponseEntity.ok(new AuthUser(jwt, authUser.getAuthType(), name));
	}
	
	/**
	 * 토큰에 있는 클레임 정보를 추출하기 
	 * @param request
	 * @return
	 */
	@RequestMapping(value= "/extract/claims", method = RequestMethod.GET )
	public ResponseEntity<?>  getExtractEmail(HttpServletRequest request) {
		
		return ResponseEntity.ok(jwtTokenUtil.getClaims(request));
	}
	
}
