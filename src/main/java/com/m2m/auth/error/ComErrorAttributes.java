package com.m2m.auth.error;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class ComErrorAttributes extends DefaultErrorAttributes {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {

   
        Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);
        
        log.debug("test");

        // jwt 토큰 문제로 인한 에러 발생(비인가) 401로 변경
        boolean jwt = ((String)errorAttributes.get("message")).matches(".*JWT.*") && (Integer)errorAttributes.get("status") == 500 ;
        if(jwt){
        	errorAttributes.put("status",401);
        	errorAttributes.put("error", "Unauthorized Error");
        }
        
        Object timestamp = errorAttributes.get("timestamp");
        if (timestamp == null) {
            errorAttributes.put("timestamp", dateFormat.format(new Date()));
        } else {
            errorAttributes.put("timestamp", dateFormat.format((Date) timestamp));
        }


        return errorAttributes;

    }

}