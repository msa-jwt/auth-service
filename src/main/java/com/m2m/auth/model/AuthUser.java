package com.m2m.auth.model;

import egovframework.com.utl.sim.service.EgovFileScrty;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Data
public class AuthUser {

    private long id;
    private String username;
    private String password;
    private String email;
    private final String jwt;
    private boolean active = true;  // 나중에 바꿀것 
    private String roles;
    private String authType = "DB"; // 인증타입, DB : database, OAUTH: oauth 인증, LDAP : ladp 인증 
    private String name;
    
    
    
    public AuthUser() {
		this.jwt = "";
    	
    }
    
    public AuthUser(String jwt, String authType, String name) {
    	this.authType = authType;
		this.jwt = jwt;
		this.name = name;
    }


    /**
     * 전자정부 프레임워크 비밀번호 암호화 모듈 
     * 사용자로 부터 입력받은 비밀번호를 전자정보 암호화 모듈을 사용하여 인코딩 함
     * 전자정보는 아래 로직에 의해 사용자 비밀번호가 암호화 되어 저장되고 있음. 
     * @return
     */

    public String getEncPw() {
    	String pw = "";
    	try {
			pw =  EgovFileScrty.encryptPassword(this.password, this.username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return pw;
    }


}