package com.m2m.auth.model;

public class SecurityConstants {
    public static final String SECRET = "93w6IFwaMf3lG3671RWzvvmkdQjJgEwqlPIz4NTib20=";
    public static final long EXPIRATION_TIME = 1000 * 60 * 30 ; // 30 minute
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
}