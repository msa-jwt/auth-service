package com.m2m.auth.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.m2m.auth.model.AuthUser;
import com.m2m.auth.model.AuthUserDetails;


import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

/**
 * jwt토큰 유틸리티
 * 토큰 생성하기
 * 토큰 validation
 * Claim 생성
 * Claim 추출
 *
 */
@Service
public class JwtUtil {

	@Value("${prop.auth.secretkey}")
    private String SECRET_KEY ;
	@Value("${prop.auth.expire.min}")
	private Integer JWT_VALID_MIN;

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
    
    public String extractEmail(String token) {
    	final Claims claims = extractAllClaims(token);
        return (String) claims.get("email");
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }
    
    public String generateTokenWithClaim(AuthUserDetails authUser) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", authUser.getUsername());
        claims.put("email", authUser.getEmail());
        return createToken(claims, authUser.getUsername());
    }

    /*
     * 토큰을 생성함 
     */
    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * JWT_VALID_MIN.intValue())) 
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY.getBytes(StandardCharsets.UTF_8)).compact();
    }

    /*
     * 토큰 유효성을 검사함 
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    
    /*
     * 클레임 정보를 추출
     */
    public AuthUser getClaims(HttpServletRequest request) {
        final String authorizationHeader = request.getHeader("Authorization");

        String username = null;
        String jwt = null;
        
        AuthUser authUser = new AuthUser();

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
            authUser.setUsername(extractUsername(jwt));
            authUser.setEmail(extractEmail(jwt));
        }
        return authUser;
    }
    

}