# Spring JWT token
Springboot의 websecurity를 사용하여 jwt 인증 서비스를 구현 하였다.


### egovframework 연계
전자정브 프레임워크를 연계하기 위해서는 egovframework를 설치해야 한다.

<a href='https://gitlab.com/msa-jwt/egovframework/-/blob/master/README.md'>전자정부 프레임워크 설치하기</a>

### db 설정 변경
egovframework  설치된 디비 정보를 변경한다.
application.yml파일을 열어서 아래 부분을 수정한다.   
```properties
    url: ${DB_URL:jdbc:log4jdbc:mysql://173.193.79.161:32455/egovdb?useUnicode=true&characterEncoding=utf-8}
    username: ${DB_USER:userid}
    password: "${DB_PW:password}"
```

### jwt token 수정
토큰에서 사용할 security 키를 저장한다.   
토큰 유효시간을 설정한다.   
```properties
prop: 
  auth:
    secretkey: "${JWT_SECRET_KEY:9qlPIz4NTib20=}"
    expire:
      min: 10      # token expire time
```

  
  

### token생성 url은 스프링 시큐러티에서 permit all로 변경함
```java
httpSecurity
        .authorizeRequests()
            .antMatchers("/api/v1/auth/token/create").permitAll()
```


# 배포시 환경설정 정의
oauth의 경우 external service를 호출해야 한다. external service주소는 환경변수에서 정의하여 참조하도록 한다.
환경변수 명은 다음의 명칭으로 생성하여 환경변수에 저장하면 된다.
```properties
${OAUTH_URL}
```